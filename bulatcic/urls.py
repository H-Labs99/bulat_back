"""bulatcic URL Configuration

"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from rest_framework.authtoken import views as tokenViews
from rest_framework.routers import DefaultRouter
from user_man.admin import admin_site
# from user_man import views, admin_views
from user_man.views import *

# Un router qui va patcher mes viewsets.
router = DefaultRouter()
router.register('users', UserViewSet, basename='users')
router.register('users_profils', ProfileViewSet, basename='users_profils')
router.register('salons', SalonViewSet, basename='salons')
router.register('etudiants-salons', EtudiantSalonViewSet, basename='etudiants salons')
router.register('invitations', SalonViewSet, basename='invitations')

urlpatterns = [
    path('api/login/', UserLogin.as_view(), name='login'),
    path('admin/', admin.site.urls),
    # url(r'^admin/', include(admin_site.urls)),
    path('api/', include(router.urls)),
    # path('api/auth', include('rest_framework.urls')),
    # path('api/get_token/', tokenViews.obtain_auth_token),
]
